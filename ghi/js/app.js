function createCard(name, description, pictureUrl, date, location) {
    const formattedDate = new Date(date).toLocaleDateString('en-US', {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    });

    return `
      <div class="card d-flex shadow p-3 mb-5 bg-white rounded mh-50 my-50 mx-50">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body d-flex flex-column flex-grow-1">
          <h5 class="card-title">${name}</h5>
          <p class="card-text>${location}</p>
          <p class="card-text flex-grow-1">${description}</p>
          <hr>
          <p class="card-text mb-0">${formattedDate}</p>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        return Promise.reject(new Error('Bad response from server'));
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const date = details.conference.starts
            const location = details.conference.location.name
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, date, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e)
    }

  });
