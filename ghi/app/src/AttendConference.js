import React, { useEffect, useState } from 'react'

export default function AttendConference() {
    const [conferences, setConferences] = useState([]);
    const [formData, setFormData] = useState( {
        name: '',
        email: '',
        conference: ''
    });
    const [selectedConference, setSelectedConference] = useState(null);

    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        if (inputName === 'conference') {
            setSelectedConference(value);
        }
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const attendeesUrl = `http://localhost:8001/${selectedConference}attendees/`
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const realResponse = await fetch(attendeesUrl, fetchConfig);
        if (realResponse.ok) {
            setFormData({
                name: '',
                email: '',
                conference: ''
            })
        }





    }
    useEffect(() => {
        fetchData();
    }, []);


  return (
    <div className="my-5 container">
        <form onSubmit={handleSubmit} id="create-attendee-form">
            <h1 className="card-title">It's Conference Time!</h1>
            <p className="mb-3">
                Please choose which conference
                you'd like to attend.
            </p>
            <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                <span className="visually-hidden">Loading...</span>
                </div>
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange}  name="conference" id="conference" className="form-select" required>
                <option value="">Choose a conference</option>
                {conferences.map((conference) => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    );
                })}
                </select>
            </div>
            <p className="mb-3">
                Now, tell us about yourself.
            </p>
            <div className="row">
                <div className="col">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" value={formData.name} />
                    <label htmlFor="name">Your full name</label>
                </div>
                </div>
                <div className="col">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" value={formData.email} />
                    <label htmlFor="email">Your email address</label>
                </div>
                </div>
            </div>
            <button className="btn btn-lg btn-primary">I'm going!</button>
        </form>
    </div>
  )
}
