import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConference from './AttendConference';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';

function App(props) {
  if (!props.attendees) {
    return (
      <>
        <h1>Hold my beer</h1>
        <img src={process.env.PUBLIC_URL + '/Alexi2.png'} alt="alex" />
      </>
    );
  }

  return (
    <BrowserRouter>
      <div className="container">
        <Nav />
        <Routes>
          <Route path='/' element={<MainPage />} />

          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>

          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />} />
          </Route>

          <Route path='attendees'>
            <Route path='new' element={<AttendConference />} />
          </Route>
          <Route path='attendees' element={<AttendeesList attendees={props.attendees} />} />

          <Route path='presentations/new' element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
