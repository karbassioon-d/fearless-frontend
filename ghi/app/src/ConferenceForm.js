import React, { useEffect, useState } from 'react'

export default function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: ''
    });

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }


    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: ''
            })
        }


    }
    useEffect(() => {
        fetchData();
    }, []);

  return (
    <div>
        <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={formData.name} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" value={formData.starts} />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" value={formData.ends} />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleFormChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" value={formData.description} ></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={formData.max_presentations} />
                <label htmlFor="Maximum presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={formData.max_attendees} />
                <label htmlFor="Maximum attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} required name="location" id="location" className="form-select" >
                  <option selected value="">Choose a location</option>
                  {locations.map((locationn) => {
                    return (
                    <option key={locationn.id} value={locationn.id}>
                        {locationn.name}
                    </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
    </div>
  )
}
